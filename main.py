from unidecode import unidecode

def compte_lettres() :
    """ Récupère un string entré par l'utilisateur, compte le nombre de char représenté dans un fichier txt lu.
        Puis renvoi un tableau de clé/ valeur correspondant aux nombres de la clé dans le string  

    Returns:
        [int, {}]: [description] return a list of an int and a dict 
    """
    compteur = 0
    liste_voyelle = ['a','e','o','u','y']  

    texte = input("Ecrire une phrase : ")

    show_voyelle = {}
    for voyelle in liste_voyelle : 
        value = 0
        for lettre in texte : 
            if lettre.lower() == voyelle : 
                compteur += 1
                value += 1
        if value > 0 :
            show_voyelle[voyelle] =  value      
            
    print("Il y a " + str(compteur) + " voyelles dans la phrase")
    print(show_voyelle)
    return compteur, show_voyelle

compte_lettres()